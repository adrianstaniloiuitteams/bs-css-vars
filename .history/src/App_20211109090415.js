
import React, {useState} from "react";
import './App.css';

function App() {
  const [bgApp, setBgApp] = useState(null);
  const [bgWidget, setBgWidget] = useState(null);
  
  const styles = {
    "--bs-body-bg": bgApp
  }

  
  return (
    <div className="App w-100 h-100" style={styles}>

<button onClick={() => setBgApp(null)}>Body Reset</button>
      <button onClick={() => setBgApp("#000")}>Body Black Mode</button>
      <button onClick={() => setBgApp("blue")}>Body Blue Mode</button>

      <button onClick={() => setBgWidget(null)}>Widget Reset</button>
      <button onClick={() => setBgWidget("#000")}>Widget Black Mode</button>
      <button onClick={() => setBgWidget("blue")}>Widget Blue Mode</button>
      <a href="#">APP</a>

    </div>
  );
}

export default App;
