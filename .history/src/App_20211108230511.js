
import React, {useState} from "react";
import './App.css';

function App() {
  const [bg, setBg] = useState('#fff');

  const styles = {
    "--bs-primary": bg
  }

  
  return (
    <div className="App" style={styles}>
      <button>Black Mode</button>
      <a href="#">aaa</a>
    </div>
  );
}

export default App;
