
import React, {useState} from "react";
import './App.css';

function App() {
  const [bgApp, setBgApp] = useState(null);
  const [bgWidget, setBgWidget] = useState(null);
  
  const styleApp = {
    "--bs-body-bg": bgApp
  }

  const styleWidget = {
    "--app-widget-bg-color": bgWidget
  }

  
  return (
    <div className="App w-100 h-100" style={styleApp}>

<button onClick={() => setBgApp(null)}>Body Reset</button>
      <button onClick={() => setBgApp("#000")}>Body Black Mode</button>
      <button onClick={() => setBgApp("blue")}>Body Blue Mode</button>

      <button onClick={() => setBgWidget(null)}>Widget Reset</button>
      <button onClick={() => setBgWidget("purple")}>Widget Purple Mode</button>
      <button onClick={() => setBgWidget("blue")}>Widget Blue Mode</button>
      <div className="widget" style={styleWidget}>
          lorem ipsum dolor sit amet  consectetur adipiscing elit
      </div>

    </div>
  );
}

export default App;
