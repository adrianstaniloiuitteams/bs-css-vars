
import React, {useState} from "react";
import './App.css';

function App() {
  const [bg, setBg] = useState(null);
  const [bgBody, setbgBody] = useState(null);
  
  const styles = {
    "--bs-primary": bg
  }

  
  return (
    <div className="App" style={styles}>
      <button onClick={() => setBg(null)}>Widget Reset</button>
      <button onClick={() => setBg("#000")}>Widget Black Mode</button>
      <button onClick={() => setBg("blue")}>Widget Blue Mode</button>
      <a href="#">APP</a>

      <button onClick={() => setBg(null)}>Body Reset</button>
      <button onClick={() => setBg("#000")}>Body Black Mode</button>
      <button onClick={() => setBg("blue")}>Body Blue Mode</button>
    </div>
  );
}

export default App;
