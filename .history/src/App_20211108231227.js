
import React, {useState} from "react";
import './App.css';

function App() {
  const [bg, setBg] = useState(null);

  const styles = {
    "--bs-primary": bg
  }

  
  return (
    <div className="App" style={styles}>
      <button onClick={() => setBg("#000")}>Black Mode</button>
      <button onClick={() => setBg("blue")}>Blue Mode</button>
      <a href="#">APP</a>
    </div>
  );
}

export default App;
