
import React, { useState } from "react";
import './App.css';
import Widget from "./Widget";

function App() {
  const [bgApp, setBgApp] = useState(null);

  const style = {
    "--bs-body-bg": bgApp
  }

  return (
    <div className="App w-100 vh-100" style={style}>

      <button onClick={() => setBgApp(null)}>Body Reset</button>
      <button onClick={() => setBgApp("black")}>Body Black Mode</button>
      <button onClick={() => setBgApp("blue")}>Body Blue Mode</button>

      <Widget />

    </div>
  );
}

export default App;
