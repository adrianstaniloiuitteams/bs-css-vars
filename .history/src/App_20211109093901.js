
import React, { useState } from "react";
import './App.css';
import Widget from "./Widget";
import {Button} from 'react-bootstrap';

function App() {
  const [bgApp, setBgApp] = useState(null);

  const style = {
    "--bs-body-bg": bgApp
  }

  return (
    <div className="App w-100 vh-100 p-5" style={style}>
      <Button onClick={() => setBgApp(null)}>Body Reset</Button>
      <Button onClick={() => setBgApp("black")}>Body Black Mode</Button>
      <Button onClick={() => setBgApp("blue")}>Body Blue Mode</Button>
      <Widget />
    </div>
  );
}

export default App;
