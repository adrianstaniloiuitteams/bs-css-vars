import React, { useState } from "react";
import { Card } from "react-bootstrap";
import {Button} from 'react-bootstrap';
import "./style.scss";

const Widget = () => {
  const [bgWidget, setBgWidget] = useState(null);

  const style = {
    "--app-widget-bg-color": bgWidget,
    "width": "18rem"
  }


  return (<>
    <Card className="widget mb-4 mt-4" style={style}>
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Card Subtitle</Card.Subtitle>
        <Card.Text>
          Some quick example text to build on the card title and make up the bulk of
          the card's content.
        </Card.Text>
        <Card.Link href="#">Card Link</Card.Link>
        <Card.Link href="#">Another Link</Card.Link>
      </Card.Body>
    </Card>
    <Button className="me-2" onClick={() => setBgWidget(null)}>Widget Reset</Button>
    <Button className="me-2" onClick={() => setBgWidget("purple")}>Widget Purple Mode</Button>
    <Button onClick={() => setBgWidget("orange")}>Widget Orange Mode</Button>
  </>
  );
};


export default Widget;